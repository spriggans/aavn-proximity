﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using aavn.proximity.Common;
using aavn.proximity.Common.Utilities;
using aavn.proximity.Services;
using aavn.proximity.Services.Request;
using aavn.proximity.Services.Response;

namespace aavn.proximity.ViewModels
{
    public class PlanetListViewModel : ViewModelBase
    {
        private ObservableCollection<PlanetViewModel> _planets;

        public ObservableCollection<PlanetViewModel> Planets
        {
            get => _planets;
            set
            {
                _planets = value;
                OnPropertyChanged(nameof(Planets));
            }
        }

        public PlanetListViewModel()
        {
            _planets = new ObservableCollection<PlanetViewModel>();
            Task.Run(async () => await LoadData());
        }

        public async Task LoadData()
        {
            var authToken = await ServiceManager.GetAuthToken(new TokenRequest
            {
                email = ResourcesManager.Instance.Email,
                passphrase = ResourcesManager.Instance.Passphrase
            });

            if (authToken.Success)
            {
                var planetResponse = await ServiceManager.GetPlanets(new PlanetRequest { AuthToken = authToken.dont_tell_anyone_this_token });
                if (planetResponse.Any())
                {
                    Planets = new ObservableCollection<PlanetViewModel>(planetResponse.Select(m => new PlanetViewModel(m)));
                }
            }
        }
    }
}
