﻿using System;
using aavn.proximity.Services.Response;

namespace aavn.proximity.ViewModels
{
    public class PlanetViewModel : ViewModelBase
    {
        private string _name;
        private string _picture { get; set; }
        private string _description { get; set; }

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        public string Picture
        {
            get => _picture;
            set
            {
                _picture = value;
                OnPropertyChanged(nameof(Picture));
            }
        }

        public string Description
        {
            get => _description;
            set
            {
                _description = value;
                OnPropertyChanged(nameof(Description));
            }
        }

        public PlanetViewModel() { }

        public PlanetViewModel(PlanetResponse response)
        {
            Name = response.name;
            Description = response.description;
            Picture = response.pic;
        }
    }
}
