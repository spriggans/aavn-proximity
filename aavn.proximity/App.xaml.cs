﻿using System;
using aavn.proximity.Pages;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace aavn.proximity
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new Planets();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
