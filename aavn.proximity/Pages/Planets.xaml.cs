﻿using System;
using System.Collections.Generic;
using aavn.proximity.ViewModels;
using Xamarin.Forms;

namespace aavn.proximity.Pages
{
    public partial class Planets : ContentPage
    {
        public Planets()
        {
            InitializeComponent();
            BindingContext = new PlanetListViewModel();
        }
    }
}
