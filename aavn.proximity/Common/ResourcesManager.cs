﻿using System;
using aavn.proximity.Properties;

namespace aavn.proximity.Common
{
    public class ResourcesManager
    {
        private readonly static ResourcesManager _instance = new ResourcesManager();
        public string Email { get; set; }
        public string Passphrase { get; set; }
        public string TokenEndpoint { get; set; }
        public string PlanetsEndpoint { get; set; }

        private ResourcesManager()
        {
            Email = Resources.email;
            Passphrase = Resources.passphrase;
            TokenEndpoint = Resources.TokenEndpoint;
            PlanetsEndpoint = Resources.PlanetsEndpoint;
        }

        public static ResourcesManager Instance
        {
            get
            {
                return _instance;
            }
        }
    }
}
