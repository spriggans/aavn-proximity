﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Newtonsoft.Json;

namespace aavn.proximity.Common.Utilities
{
    public class RestService
    {
        protected readonly ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public async Task<T> Execute<T, U>(U modelRequest, T modelResponse, string UriBase, string controller, TypeVerb verb, bool needAuth = false, string tokenAuth = "")
        {
            Logger.Info($"Se realiza petición con Execute a UriBase:{UriBase} Controller:{controller} Verb:{verb}");
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

                using (var client = new HttpClient())
                {
                    client.Timeout = new TimeSpan(0, 10, 0);
                    if (needAuth)
                    {
                        client.DefaultRequestHeaders.Add("Authorization", tokenAuth);
                    }

                    client.Timeout = new TimeSpan(0, 10, 0);
                    var request = JsonConvert.SerializeObject(modelRequest);
                    Logger.Debug($"request:{request}");
                    var content = new StringContent(request, Encoding.UTF8, "application/json");
                    //client.BaseAddress = new Uri(UriBase);

                    if (verb == TypeVerb.POST)
                    {
                        var postTaskresult = await client.PostAsync(controller, content);
                        var model = await postTaskresult.Content.ReadAsStringAsync();
                        modelResponse = JsonConvert.DeserializeObject<T>(model);
                        Logger.Debug($"response:{modelResponse}");
                    }

                    if (verb == TypeVerb.GET)
                    {
                        var getTaskresult = client.GetAsync(controller);
                        getTaskresult.Wait();
                        var getTaskContent = getTaskresult.Result;
                        var model = await getTaskContent.Content.ReadAsStringAsync();
                        modelResponse = JsonConvert.DeserializeObject<T>(model);
                        Logger.Debug($"response:{modelResponse}");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return (modelResponse);
        }
    }

    public enum TypeVerb
    {
        POST,
        GET
    }
}
