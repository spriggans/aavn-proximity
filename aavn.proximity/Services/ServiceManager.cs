﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using aavn.proximity.Common;
using aavn.proximity.Common.Utilities;
using aavn.proximity.Services.Request;
using aavn.proximity.Services.Response;

namespace aavn.proximity.Services
{
    public class ServiceManager
    {
        public static async Task<TokenResponse> GetAuthToken(TokenRequest request)
        {
            var tokenResponse = new TokenResponse();
            try
            {
                tokenResponse = await new RestService().Execute
                    (
                        request,
                        tokenResponse,
                        string.Empty,
                        ResourcesManager.Instance.TokenEndpoint,
                        TypeVerb.POST
                    );
                if (!string.IsNullOrWhiteSpace(tokenResponse.dont_tell_anyone_this_token))
                {
                    tokenResponse.Success = true;
                    return tokenResponse;
                }
                return tokenResponse;
            }
            catch(Exception ex)
            {
                ///TODO log mensaje de error
                return tokenResponse;
            }
        }

        public static async Task<IList<PlanetResponse>> GetPlanets(PlanetRequest request)
        {
            var response = new List<PlanetResponse>();
            try
            {
                response = await new RestService().Execute
                    (
                        request,
                        response,
                        string.Empty,
                        ResourcesManager.Instance.PlanetsEndpoint,
                        TypeVerb.GET,
                        true,
                        request.AuthToken

                    );

                return response;
            }
            catch (Exception ex)
            {
                ///TODO log mensaje de error
                return response;
            }
        }
    }
}
