﻿using System;
namespace aavn.proximity.Services.Request
{
    public class TokenRequest
    {
        public string email { get; set; }
        public string passphrase { get; set; }
    }
}
