﻿using System;
namespace aavn.proximity.Services.Response
{
    public class PlanetResponse
    {
        public string name { get; set; }
        public string pic { get; set; }
        public string description { get; set; }
    }
}
