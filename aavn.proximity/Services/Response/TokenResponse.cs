﻿using System;
namespace aavn.proximity.Services.Response
{
    public class TokenResponse
    {
        public string hi { get; set; }
        public string dont_tell_anyone_this_token { get; set; }
        public bool Success { get; set; }
    }
}
